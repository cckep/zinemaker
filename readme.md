
# ZINEMAKER

#### Twitter Bot


- [ ] It should run autonomously on a Raspberry Pi
- [ ] It should post and reply to mentions
- [ ] It should 'Like' and 'Follow' posts/accounts that are similar
- [x] Content should be templated
- [x] Content should be banked


Use a simple interval timer for now

V2:
- Scrape and compare:
- Id
- Genre/ Topic
- Series
- Engagement
- Time
- Date

Use Spacy and Wiki to validate Comments
Write templates:

- Which movie stars these 3 actors?
- Which Actor starred in these 3 movies?
- This movie was released on {DATE}?
- Check out the new Trailer for {MOVIE}!
- {MOVIE} is released today
- Weekend box office earnings
- {Movie} profit fact
- These 3 movies are now streaming on Netflix/Hulu/...
- These 3 movies will leave Netflix on {DATE}
- The color palette of {MOVIE} + image
- The script for {MOVIE} is ## Pages long
- Which movie had the longer script?
- Which movie has the longer runtime?
- Which movie made more profit?
- Which actor has been in more movies?
- Which movie has the larger on-screen cast?
- Pricing error: {MOVIE} is only $$
- Price drop: {MOVIE} is only $$
- {ACTOR} just signed on to this movie
- Top 5 {GENRE} movies from {YEAR}:
