import requests
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
from bs4 import BeautifulSoup as bs
from Modules.zm_Structure import DataStructure
from random import randint

class NameThatPersonClass():

    def __init__(self):
        super(NameThatPersonClass, self).__init__()
        self.star_sign = ['aquarius','pisces','aries','taurus','gemini',
                'cancer','leo','virgo','libra','scorpio','sagittarius','capricorn']
        self.gender = ['male','female']
        self.job = { 'Director': 'directed', 'Writer':'wrote','Producer':'produced',
                'Cinematographer': 'shot', 'Camera and Electrical Department': 'worked on',
                'Editor':'edited', 'Sound department':'worked on', 'Miscellaneous Crew':'worked on',
                'Actor':'acted in', 'Visual effects': 'worked on', 'Second Unit Director or Assistant Director':'directed',
                'Thanks':'was thanked for', 'Self':'was themself in', 'Archive footage':'worked on'}
        self.url_imdb = 'https://www.imdb.com/name/'
        self.url_imdb_name_search = 'https://www.imdb.com/search/name?'
        self.s = requests.Session()
        self.retries = Retry(total=5, backoff_factor=0.1, status_forcelist=[500,502,503,504])
        self.s.mount('http://', HTTPAdapter(max_retries=self.retries))

    def __GetAllNames(self, soup):
        soup_main = soup.find_all('h3', class_='lister-item-header')
        links = {}
        for link in soup_main:
            j = str(link.a)
            person = j[(j.find('>')+1):len(j)-(j[::-1].find('<')+1)].strip()
            person_id = j[j.find('nm'):j.find('>')-1]
            links.update({person: person_id})
        return links

    def __GetPerson(self, soup):
        l = self.__GetAllNames(soup)
        return list(l.items())[randint(0,len(l)-1)]

    def __GetAccolades(self, person):
        bio = []
        bio_url = self.url_imdb + person[1]
        bio_html = self.s.get(bio_url)

        b_soup = bs(bio_html.text, 'html.parser')
        roles = {}
        b_soup_main = b_soup.find_all('div', class_='knownfor-title-role')
        for i in b_soup_main:
            j = str(i.text.strip())
            movie = j.split('\n')[0]
            role = j.split('\n')[1]
            roles.update({movie:role})
        return roles

    def __GetQuote(self, movie):
        q = self.__GetAllQuotes(movie)
        if len(q) > 0:
            return str(q[randint(0,len(q)-1)])
        else:
            return 'No Quote'

    def __NameThatPerson(self, r, count): #, movie):

        # Get Roles down to 3, update actors
        _r = {} #new dict with flipped roles:movies
        for i,k in zip(r, range(count)):
            j = ''
            if r[i] in self.job:
                j = self.job[r[i]]
            else:
                j = 'acted in'

            if j in _r:
                _r[j].append(i)
            else:
                _r.update({j:[i]})
        trivia = 'Who '

        for i, k in zip(_r, range(len(_r))):
            if k == 0:
                a = ''
            elif k == len(_r)-1:
                a = ', and '
            else:
                a = ', '
            # 2 items does not have an '&'
            trivia += a + i + ' ' + ', '.join(str(m) for m in _r[i][:-1]) + (' & ' * int(len(_r[i])>1)) + str(_r[i][-1])

        trivia += '?'

        return trivia #[trivia, movie[0]]

    def GetSign(self):
        num_of_signs = 1+ randint(0,len(self.star_sign)-2)
        signs = []
        for i in range(num_of_signs):
            signs.append(self.star_sign[randint(0,len(self.star_sign)-1)])
        signs = list(set(signs))
        return signs

    def Name(self):
        return'NTM'

    def GetPostText(self):
        post = DataStructure()

        print('------NTP------NTP------NTP------NTP')
        print('called the IMDB Scraper')

        sign = self.GetSign()
        print('Signs: %s' %(sign))
        gender = self.gender[randint(0,len(self.gender)-1)]
        print('Gender: %s' %(gender))

        search = 'star_sign=' + ','.join(str(s) for s in sign) + '&gender=' + gender
        print('Search: %s' %(search))

        url = self.url_imdb_name_search + search # + '&view=simple'
        print('URL: %s' %(url))

        html = self.s.get(url)
        #html = requests.get(url)
        soup = bs(html.text, 'html.parser')
        c = 3 # actor count limit
        person = self.__GetPerson(soup)
        print(person)

##        #quote = self.__GetQuote(movie)
        roles = self.__GetAccolades(person)
        ntp = self.__NameThatPerson(roles, c)#, movie)
        post.Update(TEXT=ntp, GENRE = 'GENERIC', SERIES = 'NTP', COMEBACK=person[0])

        return(post)
