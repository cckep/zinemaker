import requests
from requests.packages.urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
from bs4 import BeautifulSoup as bs
from Modules.zm_Structure import DataStructure
from random import randint

class NameThatMovieClass():

    def __init__(self):
        super(NameThatMovieClass, self).__init__()
        self.genres = ['adventure','animation','biography','comedy','crime',
                  'drama','family','fantasy','film-noir',
                  'game-show','history','horror','music','musical','mystery',
                  'news','reality-tv','romance','sci-fi','short','sport',
                  'superhero','talk-show','thriller','war','western']
        self.url_imdb = 'https://www.imdb.com/title/'
        self.url_imdb_genre = 'https://www.imdb.com/search/title?genres='
        self.s = requests.Session()
        self.retries = Retry(total=5, backoff_factor=0.1, status_forcelist=[500,502,503,504])
        self.s.mount('http://', HTTPAdapter(max_retries=self.retries))

    def __GetAllMovieIDs(self, soup):
        soup_main = soup.find_all('div', class_='col-title')
        links = {}
        for link in soup_main:
            j = str(link.a)
            title = j[(j.find('>')+1):len(j)-(j[::-1].find('<')+1)]
            title_id = j[j.find('tt'):j.find('?')-1]
            links.update({title: title_id})
        return links

    def __GetMovie(self, soup):
        l = self.__GetAllMovieIDs(soup)
        return list(l.items())[randint(0,len(l)-1)]

    def __GetActors(self, movie, count):
        actors = []
        actor_url = self.url_imdb + movie[1]
        actor_html = self.s.get(actor_url)
        #actor_html = requests.get(actor_url)
        a_soup = bs(actor_html.text, 'html.parser')
        a_soup_main = a_soup.find_all('td', class_='itemprop')
        for a in a_soup_main:
            actors.append(a.text.strip())
            #print('actor: %s' % a.text.strip())
        return actors[:count]

    def __GetAllQuotes(self, movie):
        quote_url = self.url_imdb + movie[1]+'/quotes'
        quote_html = self.s.get(quote_url)
        #quote_html = requests.get(quote_url)
        q_soup = bs(quote_html.text, 'html.parser')
        q_soup_main = q_soup.find_all('div', class_='sodatext')
        quotes = []
        for quote in q_soup_main:
            quotes.append(quote.text)
        return quotes

    def __GetQuote(self, movie):
        q = self.__GetAllQuotes(movie)
        if len(q) > 0:
            return str(q[randint(0,len(q)-1)])
        else:
            return 'No Quote'

    def __NameThatMovie_Actors(self, actors, count): #, movie):
        trivia = 'Which Movie or TV Show Stars '

        if count > len(actors) : count = len(actors)

        for n in range(count):
            if n == (count-1): g = '?'
            elif n == (count-2): g = ', and '
            else: g = ', '
            trivia += (actors[n] + g)

        return trivia #[trivia, movie[0]]

    def Name(self):
        return'NTM'

    def GetPostText(self):
        post = DataStructure()

        print('------NTM------NTM------NTM------NTM')
        print('called the IMDB Scraper')

        genre = self.genres[randint(0,len(self.genres)-1)]

        print('Genre: %s' %(genre))
        url = self.url_imdb_genre + genre + '&view=simple'

        html = self.s.get(url)
        #html = requests.get(url)
        soup = bs(html.text, 'html.parser')

        c = 3 # actor count limit
        movie = self.__GetMovie(soup)
        print(movie)
        #quote = self.__GetQuote(movie)
        actors = self.__GetActors(movie, c)
        ntm = self.__NameThatMovie_Actors(actors, c)#, movie)
        post.Update(TEXT=ntm, GENRE = genre, SERIES = 'NTM', COMEBACK=movie[0])

        return(post)
