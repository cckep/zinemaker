#import os
from random import choice
from Templates.zm_NameThatMovie import NameThatMovieClass as NTM
from Templates.zm_NameThatPerson import NameThatPersonClass as NTP

class TemplateManagerClass():

    def __init__(self):
        super(TemplateManagerClass, self).__init__()
        self.templates = {
        'NameThatMovie': NTM(),
        'NameThatPerson': NTP()
        }

    def Get_Template(self, k=''):
        if self.templates.get(k):
            return self.templates[k]
        return self.templates[choice(list(self.templates.keys()))]
#        return self.templates[randint(0, len(self.templates)-1)]
