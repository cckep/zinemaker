import sys
import os
import threading
from Modules.zm_FolderStructure import FolderStructureClass

folder = FolderStructureClass(os.path.dirname(__file__))
sys.path.append(folder.f_auth)

from Modules.zm_calendar import CalendarClass
from Modules.Twitter.zm_Listener import StreamClass
from Modules.Twitter.zm_Publish import PublishClass
from Modules.Twitter.zm_Mentions import MentionClass
from Modules.zm_FillBank import FillBankClass
from Modules.zm_Restock import RestockClass
from Modules.Twitter.zm_api import APIClass
from auth_keys import Get_Keys

#sn = 'kanyewest'
sn = 'cinectio'
search = [sn]
listener_running = False
publish_primed = False
reply_primed = False
fill_primed = False
restock_primed = False


apiClass = APIClass(Get_Keys())
api = apiClass.Get_API()
master_calendar = CalendarClass(basic_triggers = True)
publisher = PublishClass(api, folder.f_publish, folder.f_archive)
mentions = MentionClass(folder) #folder.f_mention, folder.f_archive)
the_bank = FillBankClass(folder.f_banked_posts, folder.f_templates)
restock = RestockClass(folder.f_banked_posts, folder.f_publish)

try:
    print('setting up listener...')
    listener = StreamClass(api, folder.f_mention, sn, search)
    ears = threading.Thread(name='listener',target=listener.set_stream_listener)
    ears.start()
    print('Listener Started')
except:
    print('<!> - Unable To Set Up Listener')

#api.update_status("Alright, I'm back!")

try:
    while True:
        event = master_calendar.Triggered_Event()
        if event != [None]:
            for e in event:
                if e == 'PUBLISH':
                    print('Publish ALL Replies and Posts')
                    #publisher.Publish()
                    publish_primed = True

                elif e == 'REPLY':
                    print('Write replies')
                    #mentions.Respond_To_Mentions()
                    reply_primed = True

                elif e == 'FILL':
                    #the_bank.Fill_Bank()
                    print('Fill Bank')
                    fill_primed = True

                elif e == 'RESTOCK': # Moves series posts to 'PUBLISH' folder
                    #restock.Move_Next_Post()
                    print('Move Scheduled posts to PUBLISH folder')
                    restock_primed = True

        if publish_primed: publish_primed = publisher.Publish()
        if reply_primed: reply_primed = mentions.Respond_To_Mentions()
        if fill_primed: fill_primed = the_bank.Fill_Bank()
        if restock_primed: restock_primed = restock.Move_Next_Post()

except KeyboardInterrupt:
    print('Manual Break By User, Cleaning Threads')
    api.update_status("Not currently responding. I'll be back!")
    try:
        listener.kill_stream() # = False
        ears.join()
        print('Threads Cleaned')
    except:
        print('<!> - Unable to Clean Streams')
