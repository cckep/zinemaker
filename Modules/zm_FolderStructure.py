import os

class FolderStructureClass():

    def Make_Folder(self, path):
        if not os.path.isdir(path):
            print('Creating Folder')
            os.mkdir(path)
        return path

    def __init__(self, project_folder):
        #dir = os.path.abspath(os.curdir)
        #print(os.path.abspath(os.pardir))
        dirs = os.path.abspath(os.curdir)
        os.chdir(os.pardir)
        dirs_auth = os.path.abspath(os.curdir)
        print(dirs)
        print(dirs_auth)

        self.f_mention = self.Make_Folder(os.path.join(dirs,'Mentions'))
        self.f_reply = self.Make_Folder(os.path.join(dirs,'Replies'))
        self.f_publish = self.Make_Folder(os.path.join(dirs,'Publish'))
        self.f_archive = self.Make_Folder(os.path.join(dirs,'Archive'))
        self.f_banked_posts = self.Make_Folder(os.path.join(dirs,'Banked_Posts'))
        self.f_templates = self.Make_Folder(os.path.join(dirs,'Templates'))
        self.f_archive_mention = self.Make_Folder(os.path.join(dirs,'ArchiveMentions'))
        self.f_auth = self.Make_Folder(os.path.join(dirs_auth,'Auth'))

        super(FolderStructureClass, self).__init__()
