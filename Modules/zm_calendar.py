import datetime
import time
from random import randint
import csv
import os


class CalendarClass():
    """Keeps Track of Time, alarms, and dates. Can SCHEDULE"""

    def Sleep(self, t=1):
        now = datetime.datetime.now()
        for i in self.event_list:
            print(str(i['event']) + ': ' + str(i['time']-now))
        print('waiting...')
        time.sleep(t)

    def Schedule_Event(self, event, triggers_at_time, recurring, interval):
        #self.event_list.append({ 'event':event, 'time':triggers_at_time, 'recurring':recurring })
        self.event_list.append(
        {'event': event,
        'time' : triggers_at_time,
        'recurring' : recurring,
        'interval' : interval}
        )

    def __init__(self, basic_triggers = False, timer_varriance = 120, max_banked_posts=10):
        #self.template_list = Get_Templates(os.path.join(os.path.dirname(__file__),'Templates'))
        #self.banked_list =
        self.last_setup = datetime.datetime.now()
        #self.next_post_time =  Set_Alarm(add_second=14400)
        self.next_post_type = [None]
        self.varriance = timer_varriance # seconds
        self.banked_count = max_banked_posts
        self.event_list = []

        if basic_triggers:
            now = datetime.datetime.now()
            self.Schedule_Event('PUBLISH', now, True, 20)
            self.Schedule_Event('REPLY', now, True, 30)
            self.Schedule_Event('FILL', now, True, 40)#2*60*60)
            self.Schedule_Event('RESTOCK', now, True, 8*60*60)

    def Set_Alarm(self, add_seconds=120, add_error=True):
        """Sets an alarm for the future"""
        if add_error:
            return datetime.timedelta(seconds=randint(add_seconds, add_seconds + self.varriance)) + datetime.datetime.now()
        else: return datetime.timedelta(seconds=add_seconds) + datetime.datetime.now()

    def Triggered(self, add_seconds=120):
        a = Set_Alarm(add_seconds)
        while (datetime.datetime.now() <= a):
            time.sleep(1)
            print('waiting...')
        print('Triggered...')
        return True

    def Triggered_Event(self):
        """Checks the Schedule, returns an array of triggered events"""
        # Nothing to check?
        if len(self.event_list) < 1 :
            Sleep()
            return [None]

        a = []
        for e in self.event_list:
            if datetime.datetime.now() >= e['time']:
                a.append(e['event'])
                if e['recurring']:
                    # e['recurring'] = False
                    e['time'] += datetime.timedelta(seconds=e['interval'])
            # 4 hour interval triggered, reset alarm and return true
            # self.next_post_time = Set_Alarm(add_seconds=14400)
            #
        if len(a) > 0:
            return a
        else:
            self.Sleep()
            return [None]

    def Post_Scheduled():
        """Iterates through directory"""
        p = self.template_list.pop(0)
        self.template_list.append(p)
        return p

#    def Get_Templates(fpath):
#        files=[]
#        for f in listdir(fpath):
#            if isfile(join(fpath,f)) and f[:2] == 'zmt':
#                files.append(f)
#        ## Add Check for zero templates..
#        return f #[f for f in listdir(fpath) ]


    # Series, Interval, Starting_At
    # NTM, 14hrs, @1am
    #
    #
    # When is it good, to post what
    #
    # Time,Date > engagment > Series

    # if now() == Time,Date:
    #   post Series

    # Post_ID, {Hour, Day Of Week, Holiday}
