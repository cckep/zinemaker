import os

class DataStructure():
    """A STANDARD FOR TWEET CSV FILES"""

    def __init__(self):
        self.data = {
        'POST_ID':'NONE',
        'POST_INREPLYTO':'NONE',
        'GENRE':'NONE',
        'SERIES':'NONE',
        'TEXT':'NONE',
        'COMEBACK':'NONE',
        'ACTION':'NONE'
        }

    def Update(self, **kwargs):
        for k, v in kwargs.items():
            if self.data.get(k):    #Make sure it's there
                self.data[k] = v

#    def Update_From_List(self, l):
#        for i in l:
#            pass


    def Keys(self, with_delim=False, linesep=False):
        s = '^'*with_delim
        return s.join(str(p) for p in self.data.keys()) + os.linesep*linesep

    def Data(self, with_delim=False, linesep=False):
        s = '^'*with_delim
        return s.join(str(p) for p in self.data.values()) + os.linesep*linesep
