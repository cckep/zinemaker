from fuzzywuzzy import fuzz
import csv
from random import randint

class GiveYesNoClass():

    def __init__(self):

        super(GiveYesNoClass, self).__init__()
        self.correct = ["Yes", "Yup", "Yep", "Right", "You're right", "Correct", "You got it"]
        self.incorrect = ["No...", "Nope", "Incorrect", "Sorry, that's not it", "Try again"]
        self.answer = ["the answer is"]
        self.c_emoticon = [":‑)", ":)", ":-]", ":]", ":‑D", ":D", "=D", "=]", "=)",
        "^_^","(°o°)","(^_^)/","(^O^)／","(^o^)／","(^^)/","(≧∇≦)/","(/◕ヮ◕)/","(^o^)丿","(·ω·)","^ω^"]
        self.i_emoticon = [":‑(", ":(", ":-[", ":[", "=[", "=(", ":{", ":‑|", ":|"]
        self.close = ["Close!", "Almost..", "not sure...", "soo close"]
        self.favorite = "FAVORITE"
        self.unfavorite = "UNFAVORITE"
        self.follow = "FOLLOW"
        self.unfollow = "UNFOLLOW"
        self.none = "NONE"

    #sentence = c + name

    def Reply(self, score):
        if score <= 25:
            return self.Respnonse(self.incorrect)
        elif score <= 50:
            return self.Respnonse(self.incorrect)
        elif score <= 60:
            return self.Respnonse(self.close)
        elif score <= 70:
            return self.Respnonse(self.close)
        elif score <= 80:
            return self.Respnonse(self.correct)
        elif score <= 90:
            return self.Respnonse(self.correct)
        else:
            return self.Respnonse(self.correct)

    def Action(self, score):
        if score <= 60:
            return self.none
        elif score <= 65:
            return self.Respnonse(self.none)
        elif score <= 70:
            return self.Respnonse(self.none)
        elif score <= 75:
            return self.Respnonse(self.favorite)
        elif score <= 90:
            return self.Respnonse(self.favorite)
        else:
            return self.Respnonse(self.follow)

#    def GiveYesNoReply(self, score):
#        return {'TEXT':self.Reply(score), 'ACTION':self.Ation(score)}

    def Respnonse(self, x):
        return x[randint(0, len(x)-1)]

    #def Write_To_Publish(_rline, is_correct):
