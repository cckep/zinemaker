from fuzzywuzzy import fuzz
import string

class CompareTextVectorClass():

    def __init__(self):
        super(CompareTextVectorClass, self).__init__()
        self.common = ["a", "about", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "amoungst", "amount", "an", "and", "another", "any","anyhow", "anyone", "anything", "anyway", "anywhere", "are", "around", "as", "at", "back", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant", "co", "computer", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven", "else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "her's", "him", "him's", "his", "how", "however", "hundred", "i", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "it's", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "my's", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own", "part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thick", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves"]
        self.table = str.maketrans({key: None for key in string.punctuation})

    def StripCommon(self, word):
        for i in self.common:
            if i == word:
                word = ''
                break
        return str(word)

    def StripSentance(self, s):
        _st = []
        for i in s.split(' '):
            _st.append( self.StripCommon(i.lower()) )
        a = ' '.join(_st)
        if(len(a) <= 0):    # Check that it's not all gone
            a = s
        return a

    def GetSimilarityScore(self, a, b):
        # a = user input
        # b = answer
        x = b.split(':')[0] #trim answer beginning of ":"

        _sta = self.StripSentance(a.translate(self.table)) #trim 'input'
        _stb = self.StripSentance(b.translate(self.table)) #trim 'anstwer'
        _trm = self.StripSentance(x.translate(self.table)) #trim 'beginning'

        if len(a) < len(x)*0.75:
            print('too short!!')
            ans = 0
        else:
            _a = fuzz.partial_ratio(_sta,_trm)
            _b = fuzz.partial_ratio(_sta,_stb)
            ans = (_a+_b)/2
        return(ans)
