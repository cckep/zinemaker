import tweepy

class APIClass():
    """Sets up the Twitter API"""

    def __init__(self, k):
        super(APIClass, self).__init__()
        print('setting up API')
        self.auth = tweepy.OAuthHandler(k['consumer_key'], k['consumer_secret'])
        self.auth.set_access_token(k['access_token'],k['access_token_secret'])
        self.api = tweepy.API(self.auth)
        print(self.api)
        print('setup API')
        #self.id = self.api.get_user(screen_name=sn).id_str

    def Get_API(self):
        return self.api
