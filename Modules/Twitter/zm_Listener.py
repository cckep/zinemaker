import tweepy
import os
from datetime import datetime
import codecs
import csv
from Modules.zm_Structure import DataStructure

class StreamClass(tweepy.StreamListener):

    ##myStream = tweepy.Stream(auth = api.auth, listener=myStreamListener)

    #post_path = os.path.join(os.path.dirname(__file__), 'mentions')
    def __init__(self, api, mention_path, sn, search):
        print('called the listener....\n')
        super(StreamClass, self).__init__()
        self.keeprunning = True
        self.post_path = mention_path #os.path.join(os.path.dirname(__file__), 'mentions') #os.path.join(post_folder, file)
        self.search = search
        self.api = api
        self.id = self.api.get_user(screen_name=sn).id_str

        print('i am: %s id: %s' % (sn,self.id))
        print('mentions Path: %s' % (self.post_path))
        print('search: %s' % (self.search))
        print('api: %s' % (self.api))

    def __WriteTXT(self, status):
        ## Make sure the directory exists
        if not os.path.exists(self.post_path):
            os.makedirs(self.post_path)
        file = status.id_str + '.csv' #status.in_reply_to_status_id_str + '-' +
        file = os.path.join(self.post_path, file)


        #p_id = str(status.user.id_str)
        p_id = str(status.id_str)
        p_rp = str(status.in_reply_to_status_id_str)
        p_tx = str(status.text)
        _text = DataStructure()
        _text.Update(POST_ID=p_id,POST_INREPLYTO=p_rp, TEXT=p_tx)

        with codecs.open(file, 'w', 'utf-8') as post:
            #l = str(status.user.id_str) + '\n' + str(status.text)
            post.write(_text.Keys(with_delim=True, linesep=True))
            post.write(_text.Data(with_delim=True))
#            post.write('POST_ID^POST_INREPLYTO^GENRE^SERIES^TEXT^REPLY' + os.linesep)
#
#            post.write( p_id + '^' + p_rp + '^NONE^NONE^' + p_tx + '^NONE' )


    def on_status(self, status):
        #print(status.text)
        status_id = status.in_reply_to_status_id_str
        user_id = status.in_reply_to_user_id_str
        if type(status_id) is str:
            print('reply to status id: ' + status_id)
            if type(user_id) is str:
                print('repling to id: ' + user_id)
                print('repling to name: ' + self.api.get_user(status.in_reply_to_user_id).screen_name)
                #print(type(user_id))

            print('self_id: ' + self.id)
            #print(type(self.id))
            if user_id == self.id:
                ## in reply to me
                print('in reply to me =====================================..\n')
                self.__WriteTXT(status)
            else:
                print('Not in reply to me...\n')
        else:
            print('Not in reply..\n')

        return self.keeprunning

    def on_error(self, status_code):
        print('on_error...')
        if status_code == 420:
            #returning False in on_error disconnects the stream
            return False
        # returning non-False reconnects the stream, with backoff.

    def Stream(self):
        MyStreamListener()
        myStream = tweepy.Stream(auth = api.auth, listener=myStreamListener)
        myStream.filter(track=['python'])

    def kill_stream(self):
        self.keeprunning = False;

    def set_stream_listener(self):
        print('setting the stream')
        myStream = tweepy.Stream(auth = self.api.auth, listener=self) #the_stream_listener)
        myStream.filter(track=self.search)
