import tweepy
import csv
import os
import codecs
from Modules.zm_Structure import DataStructure

class PublishClass():
    """ Writes Directly to twitter """

    def __init__(self, api, publish, archive):
        self.api = api
        self.publish_folder = publish
        self.archive_folder = archive

    def __check_folder(self, fpath):
        return [os.path.abspath(os.path.join(fpath,f)) for f in os.listdir(fpath) if os.path.isfile(os.path.join(fpath,f))]

    def __update_archive(self, t_data):
        file = self.__check_folder(self.archive_folder)

        if len(file) > 1:
            print('unclear which archive to use')
            return False

        file=file[0]
        with codecs.open(file, 'a', 'utf-8') as post:
            post.write(t_data + os.linesep)
        return True

    def Publish(self):
        issues = False
        print('Posts to publish: ' + str(len(os.listdir(self.publish_folder))))
        all_pub_files = self.__check_folder(self.publish_folder)
        if(len(all_pub_files) > 0):
            for f in all_pub_files:
                #t_data = DataStructure()
                with open(f, newline='') as pub_file:
                    pub_data = csv.DictReader(pub_file, delimiter='^')
                    for p in pub_data:

                        p_in_rep = p['POST_INREPLYTO']
                        p_status = p['TEXT']
                        print(p_in_rep)
                        print(p_status)
                        try:
                            tweet = self.api.update_status(status=p_status, in_reply_to_status_id=p_in_rep, auto_populate_reply_metadata=True )

                            s=''
                            for k in pub_data.fieldnames:
                                s+=p[k]+'^'
                            s=s.rstrip('^')
                            # should we move the 'post id' to the 'POST_INREPLYTO'?/?
                            s = str(tweet.id) + s[s.find('^'):]
                            self.__update_archive(s)
                        except:
                            print('could not tweet...')
                            issues = True
                os.remove(f)
        return issues

    def Publish_With_Media(self, text):
        pass
