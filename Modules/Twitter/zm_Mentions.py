import csv
import os
import codecs
from Modules.Speech.zm_GiveYesNo import GiveYesNoClass
from Modules.zm_Structure import DataStructure
from Modules.Speech.zm_CompareTextVector import CompareTextVectorClass
from fuzzywuzzy import fuzz

class MentionClass():

    def __init__(self, folders): #f_mention, f_archive):
        super(MentionClass, self).__init__()
        self.give_yes_no = GiveYesNoClass()
        self.f_mention = folders.f_mention
        self.f_archive = folders.f_archive
        self.f_archive_mention = folders.f_archive_mention
        self.f_publish = folders.f_publish

        ## Folder to check
        ## Archive Lookup

    def __init_folder(self, fpath):
        return [os.path.abspath(os.path.join(fpath,f)) for f in os.listdir(fpath) if os.path.isfile(os.path.join(fpath,f))]


    def Write_Reply(self, correctness, _rline):
        if not os.path.exists(self.f_publish):
            os.makedirs(self.f_publish)
        file = _rline['POST_ID'] + _rline['POST_INREPLYTO'] + '.csv'
        file = os.path.join(self.f_publish, file)
        print('---------- Writing File to:')
        print(file)
        #self.text.Write_To_Publish(_rline, is_correct)
        print('correctness: ' + str(correctness))
        reply_text = self.give_yes_no.Reply(correctness)
        reply_action = self.give_yes_no.Action(correctness)

        _text = DataStructure()
        _text.Update(POST_INREPLYTO=_rline['POST_ID'], TEXT=reply_text, ACTION=reply_action)

        with codecs.open(file, 'w', 'utf-8') as post:
            post.write(_text.Keys(with_delim=True, linesep=True))
            post.write(_text.Data(with_delim=True))

    def Respond_To_Mentions(self):
        issues = False
        replies = self.__init_folder(self.f_mention)

        # Add Error Out
        if len(replies) < 1:
            return False

        archive = self.__init_folder(fpath=self.f_archive)

        for r in replies:
            with open(r, newline='') as r_file:
                for a in archive:
                    with open(a, newline='') as a_file:
                        r_file.seek(0)
                        #a_file.seek(0)
                        r_data = csv.DictReader(r_file, delimiter='^')
                        a_data = csv.DictReader(a_file, delimiter='^')

                        for _rline in r_data:
                            #if _rline['POST_ID'] != 'POST_ID':
                            for _aline in a_data:
                                #print('r_POST_INREPLYTO: ' + str(_rline['POST_INREPLYTO']))
                                #print('a_POST_ID: ' + str(_aline['POST_ID']))
                                if _aline['POST_ID'] == _rline['POST_INREPLYTO']:
                                    print('found in archive...')
                                    v = CompareTextVectorClass()
                                    score = v.GetSimilarityScore(_rline['TEXT'], _aline['REPLY'])

                                    self.Write_Reply(score, _rline)
                                    break
                        print('wasnt found i guess...')
                        #self.Write_Reply(self.Compare_Text(_rline['TEXT'], _aline['REPLY']), _rline)
                        break
            # remove replies
            #fle = os.path.basename(r)
            #os.rename(r, os.path.join(self.f_archive_mention, fle) )
            os.remove(r)
        return issues


#path = 'C:\\Users\\Ryan\\Dropbox\\PROJECTS\\zinegit\\Archive\\zm_PostHistory.csv' #Get this -- Modules\Twitter\zm_Mentions.py
#
#with open(path, newline='') as file:#
#    f = csv.DictReader(file, delimiter='^')
#    for l in f:
#        print(l['TEXT'])
