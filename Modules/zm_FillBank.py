import csv
import os
import codecs
from random import randint
from Modules.zm_Structure import DataStructure
from Templates.zm_TemplateManager import TemplateManagerClass
from datetime import datetime

class FillBankClass():
    """Selects a 'content type' and writes posts to Banked Posts"""

    def __init__(self, f_banked_posts, f_templates, max_in_bank=10):
        super(FillBankClass, self).__init__()
        self.f_banked_posts = f_banked_posts
        self.max_bank_count = max_in_bank

    def __init_folder(self, fpath):
        return [os.path.abspath(os.path.join(fpath,f)) for f in os.listdir(fpath) if os.path.isfile(os.path.join(fpath,f))]

    def __WriteTXT(self, text):
        if os.path.isdir(self.f_banked_posts):
            #file = post_folder
            file = ''
            _n = datetime.now()
            j = [_n.year, _n.month, _n.day, _n.hour, _n.minute, _n.second]
            for i in j:
                file += (str(i) + '_')
            file += '.csv'
            file = os.path.join(self.f_banked_posts, file)

            with codecs.open(file, 'w') as post:
                post.write(text.Keys(with_delim=True, linesep=True))
                post.write(text.Data(with_delim=True))

    def Fill_Bank(self):
        issues = False

        banked_files = self.__init_folder(self.f_banked_posts)
        if len(banked_files) >= self.max_bank_count:
            # no need to fill
            return False

        template = TemplateManagerClass().Get_Template() #Get_Template()
        print('template---- vv')
        print(template.Name())
        try:
            text = template.GetPostText()
            self.__WriteTXT(text)
        except:
            issues = True
        return issues


        # Select a content type
        # get data structure
        # write data to file in banked posts
