def Shell_Progress(d):
    n = 0
    while n < d:
        done = '#'*(n+1)
        todo = '-'*(d - n - 1)
        s = '|{0}|'.format(done+todo)
        if not todo:
            s+='\n'
        if n > 0:
            s = '\r'+s
        print(s, end ='')
        yield n
        n+=1

def Shell_Progress_Blind(tweet, stat):
    # i = progress
    # n = total
    n = int((i/n)*10)
    d = 10
    if n < d:
        done = '#'*(n+1)
        todo = '-'*(d - n - 1)
        s = '|{0}|'.format(done+todo)
        s = '\r'+s+str(v)
        print(s, end ='')

def update_value(v, n):
    info.update({v:n})

def PrintLine():
    x = '\r'
    for l in info:
        x += l + str(info[l]) + '\n'
    print(x)

s = 'Series Types'
e = 'Screen Name'
t = 'Tweet'
n = 'Next Check'
c = 'Cycles'
p = 'Posts Made'
h = 'Mentions Heard'
r = 'Mentions Relevant'
a = 'Mentions Addressed'


info = {
s:0,
e:'',
t:'',
n:0,
c:0,
p:0,
h:0,
r:0,
a:0,
}

# Series Types: 00
# -------------------------------------------|
# @screen_name :
# tweet
#
# -------------------------------------------|
# Next Check:           |####----------------| 3:14
# Cycles:               |26------------------|
# Posts Made:           |0-------------------|
# Mentions Heard:       |12------------------|
# Mentions Relevant:    |2-------------------|
# Mentions Addressed:   |2-------------------|
