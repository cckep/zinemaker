import os
import datetime

class RestockClass():

    def __init__(self, f_banked_posts, f_publish):
        super(RestockClass, self).__init__()
        self.f_banked_posts = f_banked_posts
        self.f_publish = f_publish

    def Move_Next_Post(self):
        issues = False
        # do we have posts to move?
        if len(os.listdir(self.f_banked_posts)) < 1:
            print('No Banked Posts to Move')
            return False

        # scan folder for oldest
        #oldest = os.path.join(self.f_banked_posts, os.listdir(f)[0])
        oldest = os.listdir(self.f_banked_posts)[0]

        # move file to publish folder
        os.rename(os.path.join(self.f_banked_posts, oldest), os.path.join(self.f_publish, oldest) )
        return issues
